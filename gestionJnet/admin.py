from django.contrib import admin

from gestionJnet.models import Clientes, Meses, Plan, Pagos
# Register your models here.
class ClientesAdmin(admin.ModelAdmin):
    list_display=("nombre", "direccion", "telefono")

class MesesAdmin(admin.ModelAdmin):
    list_display=("mes")

class PlanAdmin(admin.ModelAdmin):
    list_display=("megas", "precio")

#Cargando modelos
admin.site.register(Clientes, ClientesAdmin)
admin.site.register(Meses, MesesAdmin)
admin.site.register(Plan, PlanAdmin)
admin.site.register(Pagos)
