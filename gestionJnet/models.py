from django.db import models

# Create your models here.

class Clientes(model.Model):
    nombre=models.CharField(max_length=50)
    apellidos=models.CharField(max_length=50)
    direccion=models.CharField(max_length=30)
    telefono=models.CharField(max_length=8)
    ip=models.CharField(max_length=20)
    plan=models.CharField(max_length=10)

class Meses(model.Model):
    mes=models.CharField(max_length=15)
    cliente=models.CharField(max_length=30)
    anio=models.CharField(max_length=4)

class Plan(model.Model):
    megas=models.CharField(max_length=10)
    precio=models.IntegerField()

class Pagos(model.Model)
    total=models.IntegerField()
    cliente=models.CharField(max_length=30)
    mes=models.CharField(max_length=30)
    

